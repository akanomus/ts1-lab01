package cz.fel.cvut.ts1;

public class Main {
    public static void main(String[] args) {
        FactorialCalculator fc = new FactorialCalculator();
        System.out.println(fc.factorial(5));
    }
}
