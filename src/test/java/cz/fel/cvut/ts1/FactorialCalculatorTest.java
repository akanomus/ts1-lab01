package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FactorialCalculatorTest {

    @Test
    void factorial() {
        FactorialCalculator fc = new FactorialCalculator();
        long factorial = fc.factorial(5);
        Assertions.assertEquals(120, factorial, "Factorial is not calculated correctly");
    }

    @Test
    void factorial_0(){
        FactorialCalculator fc = new FactorialCalculator();
        long factorial = fc.factorial(0);
        Assertions.assertEquals(120, factorial, "Factorial is not calculated correctly");
    }

    @Test
    void factorial_negative_number(){
        FactorialCalculator fc = new FactorialCalculator();
        try {
            long factorial = fc.factorial(-1);
        } catch (Exception e) {
            Assertions.assertNotNull(e);
        }
    }
}